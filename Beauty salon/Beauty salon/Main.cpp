#include"Customer.h"
#include"DiscountRate.h"
#include"Visit.h"
#include<vector>
#include<string>
#include<fstream>

	std::ifstream inFile;

void checkConnection() {

	if (inFile.is_open()) {//if the file was opened before
	}
	else {
		inFile.open("Text.txt");//if the file was not open
		if (!inFile) {//if the file can not be open
			std::cerr << "Unable to open file datafile.txt";
			exit(1);   // call system to stop
		}
	}
}

std::vector<Customer> readCustomer(std::vector<Customer> myCustomer) {

	checkConnection();//test the connection
	int customerNumer;
	std::string name;
	bool member;
	std::string memberType;
	inFile >> customerNumer;
	myCustomer.resize(customerNumer);//myCustomer size was 0; resize it for the number of customers
	for (int i(0); i < customerNumer; i++) 
	{
		inFile >> name;//read, in this case, the name from the file
		inFile >> member;//same for the member, bool
		inFile >> memberType;
		if (memberType.compare("non")==0) {
			memberType = " ";//if is not member, " "
		}
		
		myCustomer[i].setName(name);
		myCustomer[i].setMember(member);
		myCustomer[i].setMemberType(memberType);

	}
	return myCustomer;
}

std::vector<Visit> readVisits(std::vector<Visit> myVector) {
	
	std::string name;
	checkConnection();
	int day, month, year;
	Date date;
	double service;
	double product;
	while (!inFile.eof()) {//while we are not at the End Of the File
		inFile >> name;
		inFile >> day >> month >> year;
		date.setDate(day, month, year);
		Visit myVist=Visit(name, date); //call the second constructor
		inFile >> service;
		inFile >> product;
		myVist.setServiceExpense(service);
		myVist.setProductExpense(product);
		myVector.push_back(myVist); //insert in my vector a new element
	}
	return myVector;
}

bool lookForTheName(std::vector<Customer> myCustomers, std::string name) 
{
	for (int i(0); i < myCustomers.capacity(); i++) {
		if (myCustomers[i].getName().compare(name) == 0&&myCustomers[i].isMember()==true)
			return true;
	}
	return false;
}

double applyTheDiscount(std::vector<Customer> myCustomers, std::string name) {
	DiscountRate dr;
	for (int i(0); i < myCustomers.capacity(); i++) {
		if (myCustomers[i].getName().compare(name) == 0) {
			if (myCustomers[i].isMember() == true) {
				return dr.getProductDiscountRate(myCustomers[i].getMemberType());
			}
		}
	}
}
int main()
{
	std::vector<Customer> myCustomer;
	myCustomer=readCustomer(myCustomer); //we read all the customers from Text.txt file
	std::vector<Visit> myVisits;
	myVisits=readVisits(myVisits);
	double myRate;
	for (int i(0); i < myVisits.capacity(); i++) {
		if(myVisits[i].isMember()==true)
			if (lookForTheName(myCustomer, myVisits[i].getName()) == true) {
				myRate = applyTheDiscount(myCustomer, myVisits[i].getName());
				myVisits[i].setProductExpense(myVisits[i].getProductExpense() - (myVisits[i].getProductExpense()*myRate));
				myVisits[i].setServiceExpense(myVisits[i].getProductExpense() - (myVisits[i].getProductExpense()*0.1));
			}
		
	}
	inFile.close();
	system("pause");
}
