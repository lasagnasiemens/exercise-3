#pragma once
#include<iostream>
#include"Customer.h"
class DiscountRate
{
private:
	const double serviceDiscountPremium = 0.2;
	const double serviceDiscountGold = 0.15;
	const double serviceDiscountSilver = 0.1;
	const double productDiscountPremium = 0.1;
	const double productDiscountGold = 0.1;
	const double productDiscountSilver = 0.1;
public:
	DiscountRate();
	double getServiceDiscountRate(std::string type) const;
	double getProductDiscountRate(std::string type) const;
	~DiscountRate();
};

