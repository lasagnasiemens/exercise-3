#pragma once
#include <iostream>
#include<vector>
#include<stdio.h>

class Customer
{
private:
	std::string name;
	bool member;
	std::string memberType;

public:
	std::string getName() const;
	void setName(std::string name);
	bool isMember() const;
	void setMember(bool member);
	std::string getMemberType() const;
	void setMemberType(std::string memberType);
	std::string toString()const;
	Customer();
	Customer(std::string name);
	~Customer();
};
