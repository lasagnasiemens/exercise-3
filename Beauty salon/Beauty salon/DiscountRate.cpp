#include "DiscountRate.h"



DiscountRate::DiscountRate()
{
}

double DiscountRate::getServiceDiscountRate(std::string serviceType) const
{
	if (serviceType.compare("premium") == 0)
	{
		return 0.2;
	}
	else {
		if (serviceType.compare("gold") == 0) {
			return 0.15;
		}
		else {
			if (serviceType.compare("silver") == 0)
				return 0.1;
			else return 0;
		}
	}
}

double DiscountRate::getProductDiscountRate(std::string type) const
{
	if (type.compare("premium") == 0 || type.compare("gold") == 0 || type.compare("silver") == 0)
		return 0.1;
	else
		return 0;
}

DiscountRate::~DiscountRate()
{
}
