#pragma once
#include"Customer.h"
#include"Date.h"


class Visit: public Customer
{
private:
	Customer myCustomer;
	Date date;
	double serviceExpense;
	double productExpense;
public:
	Visit();
	Visit(std::string name, Date date);
	std::string getName() const;
	double getServiceExpense() const;
	void setServiceExpense(double ex);
	double getProductExpense() const;
	void setProductExpense(double ex);
	double getTotalExpense() const;
	std::string toString()const;
	~Visit();
};



