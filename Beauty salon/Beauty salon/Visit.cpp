#include "Visit.h"
#include <string>

Visit::Visit()//empty ctor.
{
}

Visit::Visit(std::string name, Date d)//we can call this ctor if our def of the object is obj=Visit(name, date)
{
	myCustomer.setName(name);
	date.setDate(d.day, d.month, d.year);
}

std::string Visit::getName() const//return the name of the customer
{
	return this->myCustomer.getName();
}

double Visit::getServiceExpense() const
{
	return this->serviceExpense;
}

void Visit::setServiceExpense(double ex)
{
	this->serviceExpense = ex;
}

double Visit::getProductExpense() const
{
	return this->productExpense;
}

void Visit::setProductExpense(double ex)
{
	this->productExpense = ex;
}

double Visit::getTotalExpense() const
{
	return this->productExpense + this->serviceExpense;
}

std::string Visit::toString()const//fie vorba intre noi, chiar nu am gasit o metoda mai buna pentru asta. ma documentez, si revin cu o varianta mai accesibila
{
	std::string myString;

	myString.append(myCustomer.getName());
	myString.append(" ");
	myString.append(std::to_string(date.day));
	myString.append(" ");
	myString.append(std::to_string(date.month));
	myString.append(" ");
	myString.append(std::to_string(date.year));
	myString.append(" ");
	myString.append(std::to_string(serviceExpense));
	myString.append(" ");
	myString.append(std::to_string(productExpense));
	myString.append(" ");
	return myString;
}


Visit::~Visit()
{
}


