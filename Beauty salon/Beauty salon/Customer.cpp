#include "Customer.h"
#include<string>


Customer::Customer()
{
}

Customer::Customer(std::string n)//ctor for our object; set the name
{
	this->name = n;
	this->member = false;
	this->memberType = "";
}

Customer::~Customer()//nothing has to be deleted
{
}

std::string Customer::getName() const //get the name of "this"/current object
{
	return this->name;
}

void Customer::setName(std::string name)//set the name of "this"/current object
{
	this->name = name;
}

bool Customer::isMember() const
{
	return this->member;
}

void Customer::setMember(bool m)
{
	this->member = m;
}

std::string Customer::getMemberType() const
{
	return this->memberType;
}

void Customer::setMemberType(std::string mT)
{
	this->memberType = mT;
}

std::string Customer::toString()const//proper display of the obj
{
	std::string myString;
	if (this->member == false) 
	{
		myString.append(this->name);
		myString.append(" ");
		myString.append(" NOT a member");
	}
	else 
	{
		myString.append(this->name); 
		myString.append(" ");
		myString.append(std::to_string(this->member)); 
		myString.append(" ");
		myString.append(this->memberType);
	}
	
}



